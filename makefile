# make for stop-start docker-compose stack

ARG = -g -Wall
all:testage lenchaine

#ex1 table ascii echo lenchaine

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

ex1: ex1.c ## help with much word
	gcc $(ARG) ex1.c -o ex1

table: table.c ## help
	gcc $(ARG) table.c -o table

ascii: ascii.c ## help
	gcc $(ARG) ascii.c -o ascii

piramide: piramide.c ## help
	gcc $(ARG) piramide.c -o piramide

echo: echo.c ## help
	gcc $(ARG) echo.c -o echo

lenchaine: lenchaine.c ## help
	gcc $(ARG) lenchaine.c -o lenchaine

testage: testage.c ## help
	gcc $(ARG) testage.c -o testage


