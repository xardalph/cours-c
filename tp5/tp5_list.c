#include "list_include.h"

ol_status_t ol_insert(ordered_list_t *list, uint32_t seq)
{
    ordered_list_elem *elem = malloc(sizeof(*elem));
    elem->seq = seq;
    if (list == NULL || list->head == NULL)
    {
        #ifdef DEBUG
            printf("empty list in insert function\n");
        #endif
        return (OL_ERROR);
    }
    ordered_list_elem_t *actuel = list->head;

    if (actuel->seq > elem->seq)
    {
        list->head = elem;
        elem->next = actuel;
        list->size += 1;
        return OL_OK;
    }

    while (actuel != NULL)
    {

        if (actuel->next == NULL)
        {
            actuel->next = elem;
            list->size += 1;
            #ifdef DEBUG
                printf("element %d added at the end of the list \t size : %d \n", elem->seq, list->size);
            #endif

            return OL_OK;
        }
        else if (actuel->seq == elem->seq || (actuel->next)->seq == elem->seq)
        {
            #ifdef DEBUG
                printf("element %d already exist ! \n", elem->seq);
            #endif
            /* on error whe need to free the elem structure or it will be lost */
            free(elem);
            return (OL_ERROR);
        }
        else if (actuel->seq < elem->seq && (actuel->next)->seq > elem->seq)
        {
            elem->next = actuel->next;
            actuel->next = elem;
            list->size += 1;
            #ifdef DEBUG
                printf("element %d \t\t\t\t size : %d\n", elem->seq, list->size);
            #endif

            return OL_OK;
        }
        else
        {
            actuel = actuel->next;
        }
    };

    return OL_ERROR;
};

ol_status_t ol_initialize(ordered_list_t *list)
{
    /*allocate first element to initalize the structure*/
    ordered_list_elem *first_list_elem = malloc(sizeof(*first_list_elem));
    first_list_elem->seq = 3;
    list->head = first_list_elem;
    list->size = 1;
    #ifdef DEBUG
        printf("%p \t : list here \n%p \t : first element \nlink made between head and element %d\ninitalisation ended successfully \n\n", list, first_list_elem, list->head->seq);
    #endif

    return 0;
}

void print_list(ordered_list_t *list)
{
    ordered_list_elem_t *actuel = list->head;

    while (actuel != NULL)
    {
        printf("%d -> ", actuel->seq);
        actuel = actuel->next;
    }
    printf("     size : %d\n", list->size);
};

void *ol_find(ordered_list_t *list, uint32_t seq)
{
    if (list == NULL || list->head == NULL)
    {
        return (NULL);
    }
    ordered_list_elem_t *actuel = list->head;
    while (actuel != NULL)
    {
        if (actuel->seq == seq)
        {
            return actuel;
        }
        actuel = actuel->next;
    }
    return NULL;
};

void *ol_find_and_remove(ordered_list_t *list, uint32_t seq)
{
    if (list == NULL || list->head == NULL)
    {
        #ifdef DEBUG
            printf("list empty or seq = NULL\n");
        #endif
        return (NULL);
    }
    ordered_list_elem_t *actuel = list->head;
    ordered_list_elem_t *next_elem = list->head;
    if (actuel->seq == seq)
    {
        list->head = actuel->next;
        free(actuel);
        return NULL;
    }

    do
    {
        if ((actuel->next)->seq == seq)
        {
            next_elem = actuel->next;
            actuel->next = next_elem->next;
            free(next_elem);
            return NULL;
        }
        actuel = actuel->next;

    } while (actuel != NULL);

    return NULL;
};

ol_status_t ol_release(ordered_list_t *list)
{
    if (list == NULL || list->head == NULL)
    {
        return (OL_ERROR);
    }
    ordered_list_elem_t *actuel = list->head;

    while (actuel != NULL)
    {
        ordered_list_elem_t *next_elem = actuel->next;
        free(actuel);
        actuel = next_elem;
    };
    list->head = NULL;
    list->size = 0;
    return OL_OK;
};

/*fonction used to print help */
void help()
{
    printf(" h \t : for help \n ls \t : list element \n add \t : add a element to the list\n");
    printf(" r \t : ask element to remove from list \n");
    printf(" 1 \t : launch test_list_1\n");
    printf(" 2 \t : launch test_list_1\n");
    printf(" 3 \t : launch test_list_1\n");
    printf(" 4 \t : launch test_list_1\n");
#ifdef DEBUG
    printf(" d \t : launch ol_check_list_coherency \n");
#endif
}

void test_list_1(ordered_list_t *list)
{
    ol_status_t return_code;
    uint32_t seq = 0;
    while (seq < 25)
    {
        seq += 1;
        return_code = ol_insert(list, seq);
        if (return_code == OL_ERROR)
        {
            printf("error for : %d\n", seq);
        }
    }

    int32_t *return_code_remove = ol_find_and_remove(list, 5);
    return_code_remove = ol_find_and_remove(list, 18);
    return_code_remove = ol_find_and_remove(list, 9);
    printf("%ls", return_code_remove);
};

/* rand() & 0xFF can't create > 256 int, so we can have an infinite loop in test_list_2 if we try to put more, 
    but ordered_list_elem->seq is a 32 bit integer, so we can store more than 256 element inside, as we see in test_list_1
*/
void test_list_2(ordered_list_t *list)
{
    uint32_t seq = (uint32_t)(rand() & 0xFF);
    uint32_t return_code = 0;
    do
    {
        seq = (uint32_t)(rand() & 0xFF);
        return_code = ol_insert(list, seq);
    } while (return_code != OL_ERROR && list->size < 10);
};

void test_list_3(ordered_list_t *list)
{
    ol_status_t return_code;
    uint32_t seq = 0;
    while (seq < 260)
    {
        seq += 1;
        return_code = ol_insert(list, seq);
        if (return_code == OL_ERROR)
        {
            printf("error for : %d\n", seq);
        }
    }
    print_list(list);
};

void test_list_4(ordered_list_t *list)
{
    ol_status_t return_code = ol_insert(NULL, 35);
    printf("ol_insert(NULL, 35) : %d\n", return_code);

    /*generate 2 warning at compile time but don't exit*/
    return_code = ol_insert(list, NULL);
    printf("ol_insert(list, NULL) : %d\n", return_code);
    print_list(list);
    /*generate 2 warning at compile time but don't exit */
    ol_find_and_remove(list, NULL);
    printf("ol_find(list, NULL)\n");
    print_list(list);
    return_code = ol_insert(list, 999);
    printf("ol_insert(NULL, 999) : %d\n", return_code);
};

#ifdef DEBUG
/*
 * For debug only: check the list coherency.
 */
ol_status_t ol_check_list_coherency(ordered_list_t *list)
{
    if (list == NULL || list->head == NULL)
    {
        exit(EXIT_FAILURE);
    }
    ordered_list_elem_t *actuel = list->head;
    uint32_t counter = 1;

    while (actuel->next != NULL)
    {

        if (actuel->seq >= (actuel->next)->seq)
        {
            printf("seq equal or superior to next element ! \t seq : %d\n", actuel->seq);
            return OL_ERROR;
        }
        else if (actuel->seq < (actuel->next)->seq)
        {
            printf("\n\t\t\t seq = %d ", actuel->seq);
            actuel = actuel->next;
            counter += 1;
        }
    }
    if (counter == list->size)
    {
        printf("\nsize OK : \t stored  %d \t counted %d \n", list->size, counter);
    }
    printf("all seem OK\n");
    return (OL_OK);
};
#endif /* DEBUG */

