#include "list_include.h"

int main(int argc, char *argv[])
{

    ordered_list *list = malloc(sizeof(*list));
    ol_status_t return_code = ol_initialize(list);
    if (return_code != OL_OK)
    {
        printf("initialisation problem\n");
        exit(EXIT_FAILURE);
    }
    char userinput[6] = "ppppp";

    int userinput_int = 0;
    do
    {
        printf("what do you want to do ? (h for help)\n");
        scanf("%4s", userinput);

        if (strcmp(userinput, "h") == 0)
        {
            help();
        }
        else if (strcmp(userinput, "ls") == 0)
        {
            print_list(list);
        }
        else if (strcmp(userinput, "add") == 0)
        {
            scanf("%d", &userinput_int);
            return_code = ol_insert(list, userinput_int);
        }
        else if (strcmp(userinput, "1") == 0)
        {
            test_list_1(list);
        }
        else if (strcmp(userinput, "2") == 0)
        {
            test_list_2(list);
        }
        else if (strcmp(userinput, "3") == 0)
        {
            test_list_3(list);
        }
        else if (strcmp(userinput, "4") == 0)
        {
            test_list_4(list);
        }
        else if (strcmp(userinput, "r") == 0)
        {
            printf("which number do you want to remove ?\n");
            scanf("%32d", &userinput_int);
            ol_find_and_remove(list, userinput_int);
        }
#ifdef DEBUG
        else if (strcmp(userinput, "d") == 0)
        {
            ol_status_t return_code = ol_check_list_coherency(list);
            printf("list debug : %d\n", return_code);
        }
#endif

    } while (strcmp(userinput, "q") != 0);

    return_code = ol_release(list);
    free(list);
    printf("release de la list : %d\n", list->size);
    return 0;
}
