/*
 * Licence-Pro ASSR - Introduction à C - Listes chaînées
 * Copyright (c) 2019 Vincent Roca - all rights reserved
 */
#ifndef TP_LIST_H
#define TP_LIST_H

/*
 * Global view of the structures. Example of a list with two elements.
 *
 *    +--------------+     +--------------+     +--------------+
 *    | head  -------|---->| seq = 5      |     | seq = 7      |
 *    | size = 2     |     | next  -------|---->| next = NULL  |
 *    |              |     | content -+   |     | content -+   |
 *    +--------------+     |          |   |     |          |   |
 *    ordered_list_t       +--------------+     +--------------+
 *                        ordered_list_elem_t  ordered_list_elem_t
 *                                    |                     |
 *                                    v                     v
 *                     application +-----+               +-----+
 *                        specific |     |               |     |
 *                         content +-----+               +-----+
 */

/*
 * Ordered list, simply linked, where all list members have a strictly increasing
 * sequence number.
 * Search operations follow a linear search algorithm, so this class is recommended
 * for small and medium size lists.
 */
typedef struct ordered_list ordered_list;
typedef struct ordered_list
{
        /* pointer to first element of list, with the lowest sequence number, or
         * NULL if the list is empty */
        struct ordered_list_elem *head;
        /* number of elements in list, 0 if the list is empty */
        uint32_t size;
} ordered_list_t;

/*
 * Control structure for list elements.
 * An instance of this structure needs to be allocated and initialised to each element.
 */
typedef struct ordered_list_elem ordered_list_elem;
typedef struct ordered_list_elem
{
        uint32_t seq;                   /* the sequence number used to order the list */
        struct ordered_list_elem *next; /* the next element of the list, in strictly
                                                 * increasing sequence order, NULL for the last
                                                 * element of the list */
                                        /* void *content;                   the content of the element. Opaque value, only
                                                 * meaningful to the caller of this list service */
} ordered_list_elem_t;

/*
 * Return status to functions, when applicable.
 */
typedef enum
{
        OL_OK = 0, /* everything is fine */
        OL_ERROR   /* an error occured */
} ol_status_t;

/*
 * List constructor.
 * Needs to be called upon first use in order to initialize the top level,
 * ordered_list structure.
 * The ordered_list_t structure must be allocated by the caller.
 *
 * @param list  pointer to the list structure to initialize.
 * @return      completion status: OL_OK or OL_ERROR
 */
ol_status_t ol_initialize(ordered_list_t *list);

/* List destructor.
 * Removes and deletes all the elements of the list, freeing all the allocated
 * ordered_list_elem buffers for instance.
 * The ordered_list_t structure must be freed by the caller, not this function.
 *
 * @param list  pointer to the list structure to release.
 * @return      completion status: OL_OK or OL_ERROR
 */
ol_status_t ol_release(ordered_list_t *list);

/* Inserts the element at its location (i.e. to comply with the strictly
 * increasing sequence number property) in the list.
 * This function allocates and initializes the ordered_list_elem structure
 * used internally.
 *
 * @param seq   sequence number. This sequence number must be unique.
 * @param content       opaque pointer to the content associated to that
 *              sequcne number.
 * @return      completion status: OL_OK if inserted, or OL_ERROR
 *              if already present in list.
 */

ol_status_t ol_insert(ordered_list_t *list,
                      uint32_t seq);

/* Find the element with the given sequence number in the ordered list,
 * without removing it from the list, nor freeing the ordered_list_elem
 * structure used internally.
 * The result is either a success (element found) or failure (not found).
 *
 * @param seq   sequence number to look for, assumed unique.
 * @return      pointer to the application content associated to that
 *              sequence number if found, NULL if not found in list or
 *              in case of error.
 * 
 */
void *ol_find(ordered_list_t *list, uint32_t seq);

/* Finds and removes the element with the given sequence number from the
 * list. This function frees the ordered_list_elem structure used internally.
 * The result is either a success (element found/removed) or failure
 * (not found).
 *
 * @param seq   sequence number to look for, assumed unique.
 * @return      pointer to the application content associated to that
 *              sequence number if found, NULL if not found in list or
 *              in case of error.
 */
void *ol_find_and_remove(ordered_list_t *list, uint32_t seq);

/* to simplify, we do not use the content variable
 * the variable 
 * Returns the content associated to the first element of list, without
 * removing it from the list.
 *
 * @return      pointer to the content associated to the first element,
 *              or NULL if list is empty.
 * void *ol_get_first(ordered_list_t *list);
 *
 * Returns the content associated to the last element of list, without
 * removing it from the list.
 *
 * @return      pointer to the content associated to the last element,
 *              or NULL if list is empty.
 *
 * void *ol_get_last(ordered_list_t *list); 
 * 
 * Returns the number of elements in list.
 *
 * @return      total number of elements in list.
 */
uint32_t ol_get_size(ordered_list_t *list);

#ifdef DEBUG
/*
 * For debug only: check the list coherency.
 */
ol_status_t ol_check_list_coherency(ordered_list_t *list);
#endif /* DEBUG */

#endif /* TP_LIST_H */

void print_list(ordered_list_t *list);

void help();

void test_list_1(ordered_list_t *list);
void test_list_2(ordered_list_t *list);
void test_list_3(ordered_list_t *list);
void test_list_4(ordered_list_t *list);